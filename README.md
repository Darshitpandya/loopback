```
# How to creare REST API in 20 minutes with LOOPBACK
```

```
## Prerequisites
```

```
Before starting this tutorial, you must install:
- Node.js
- LoopBack CLI tools(npm install -g loopback-cli) ; see [lb](http://loopback.io/doc/en/lb3/Installation.html)
```

```
## Procedure
```

```
### Scalfolding your application
```

```
​```
lb
... # follow the prompts
cd to your application directory
​```
```

```
### Create the datasource
```

```
​```
lb datasource
... # follow the prompts, choose `your respective dataSource`
​```
```

```
### Create the models
```

```
​```
lb model
... # follow the prompts,
​```
```

```
### Run your application
```

```
​```
node .
​```
```